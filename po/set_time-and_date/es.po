# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# German Lancheros <glancheros2015@gmail.com>, 2021
# anticapitalista <anticapitalista@riseup.net>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-08 16:56+0200\n"
"PO-Revision-Date: 2021-10-29 12:04+0000\n"
"Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021\n"
"Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: set_time-and_date.sh:20
msgid "Manage Date and Time Settings"
msgstr "Ajustar fecha y hora"

#: set_time-and_date.sh:22
msgid "Date:"
msgstr "Fecha:"

#: set_time-and_date.sh:23
msgid "Set Current Time"
msgstr "Ajustar la hora correcta"

#: set_time-and_date.sh:24
msgid "Set Current Date"
msgstr "Establecer la fecha correctal"

#: set_time-and_date.sh:25
msgid "Choose Time Zone (using cursor and enter keys)"
msgstr "Elija la zona horaria (con las flechas y la tecla Enter)"

#: set_time-and_date.sh:26
msgid "Use Internet Time server to set automaticaly time/date"
msgstr "Obtener la fecha y hora automáticamente de Internet"

#: set_time-and_date.sh:27
msgid "Move the slider to the correct Hour"
msgstr "Mueva la guía a la Hora correcta"

#: set_time-and_date.sh:28
msgid "Move the slider to the correct Minute"
msgstr "Mueva la guía al Minuto correcto"

#: set_time-and_date.sh:29
msgid "Select Time Zone"
msgstr "Seleccione la Zona Horaria"

#: set_time-and_date.sh:30
msgid "Quit"
msgstr "Salir"

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Henry Oquist <henryoquist@nomalm.se>, 2020
# anticapitalista <anticapitalista@riseup.net>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-01 11:32+0200\n"
"PO-Revision-Date: 2020-02-26 16:23+0000\n"
"Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2020\n"
"Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: fast_personal_menu_editor.sh:11 fast_personal_menu_editor.sh:26
msgid "Fast Personal Menu Manager for IceWM"
msgstr "Snabb personlig menyhanterare för IceWM"

#: fast_personal_menu_editor.sh:12
msgid "App .desktop file"
msgstr ".desktop-fil för app"

#: fast_personal_menu_editor.sh:14
msgid "REMOVE last entry"
msgstr "TA BORT senaste post"

#: fast_personal_menu_editor.sh:15
msgid "ORGANIZE entries"
msgstr "ORGANISERA poster"

#: fast_personal_menu_editor.sh:16
msgid "UNDO last change"
msgstr "TA BORT senaste ändring"

#: fast_personal_menu_editor.sh:17
msgid "ADD selected app"
msgstr "LÄGG TILL vald app"

#: fast_personal_menu_editor.sh:18
msgid ""
"Choose (or drag and drop to the field below) the .desktop file you want to "
"add to the personal menu \\n OR select any other option"
msgstr ""
"Välj (eller dra och släpp i fältet nedan) den .desktop-fil du vill lägga "
"till i den personliga menyn \\n ELLER ta en annan valmöjlighet"

#: fast_personal_menu_editor.sh:26
msgid ""
"FPM has no 'graphical' way to allow users to move icons around or delete "
"arbitrary icons.\\nIf you click OK, the personal menu configuration file "
"will be opened for editing.\\nEach menu icon is identified by a line "
"starting with 'prog' followed by the application name, icon location and the"
" application executable file.\\nMove or delete the entire line refering to "
"each personal menu entry.\\nNote: Lines starting with # are comments only "
"and will be ignored.\\nThere can be empty lines.\\nSave any changes and then"
" restart IceWM.\\nYou can undo the last change from FPMs 'Restore' button."
msgstr ""
"FPM har inget 'grafiskt' sätt att låta användarna flytta omkring ikoner "
"eller ta bort slumpmässiga ikoner.\\nOm du klickar OK, kommer den personliga"
" meny-konfigurationsfilen att vara öppen för redigering.\\nVarje menyikon "
"identifieras av en rad som börjar med 'prog' följt av programnamnet, ikonens"
" plats och programmets körbara fil.\\nFlytta eller ta bort hela den rad som "
"refererar till en personlig meny-post.\\nAnmärkning: Rader som börjar med # "
"är enbart kommentarer och kommer att ignoreras.\\nTomma rader får "
"förekomma.\\nSpara ändringar och starta sedan om IceWM.\\nDu kan ångra den "
"senaste ändringen med FPMs 'Restore' knapp."

#: fast_personal_menu_editor.sh:38 fast_personal_menu_editor.sh:42
#: fast_personal_menu_editor.sh:109
msgid "Warning"
msgstr "Varning"

#: fast_personal_menu_editor.sh:38
msgid "FTM is programmed to always keep 1 line in the personal menu file!"
msgstr ""
"FTM är programerad att alltid behålla 1 rad i den personliga menyfilen!"

#: fast_personal_menu_editor.sh:42
msgid "This will delete the last entry from your personal menu! Are you sure?"
msgstr ""
"Detta kommer att ta bort den sista posten från din personliga meny! Är du "
"säker?"

#: fast_personal_menu_editor.sh:109
msgid "No changes were made! Please choose an application."
msgstr "Inga ändringar gjordes! Var vänlig välj ett program."

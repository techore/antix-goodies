# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Panwar108 <caspian7pena@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-12-16 17:56+0200\n"
"PO-Revision-Date: 2020-02-26 16:23+0000\n"
"Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2021\n"
"Language-Team: Hindi (https://www.transifex.com/anticapitalista/teams/10162/hi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: icewm-toolbar-icon-manager.sh:13
msgid "You are running an IceWM desktop"
msgstr "आइस डब्लू-एम डेस्कटॉप प्रयुक्त है"

#: icewm-toolbar-icon-manager.sh:15 icewm-toolbar-icon-manager.sh:101
msgid "Warning"
msgstr "चेतावनी"

#: icewm-toolbar-icon-manager.sh:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr "यह स्क्रिप्ट केवल आइस डब्लू-एम डेस्कटॉप में निष्पादन हेतु ही है"

#: icewm-toolbar-icon-manager.sh:42
msgid "No localized name found, using the original one"
msgstr "कोई अनुवादित नाम नहीं मिला, वास्तविक नाम उपयोग होगा"

#: icewm-toolbar-icon-manager.sh:58 icewm-toolbar-icon-manager.sh:67
#: icewm-toolbar-icon-manager.sh:83 icewm-toolbar-icon-manager.sh:115
#: icewm-toolbar-icon-manager.sh:135
msgid "Toolbar Icon Manager"
msgstr "साधन-पट्टी आइकन प्रबंधक"

#: icewm-toolbar-icon-manager.sh:58
msgid "Help::TXT"
msgstr "सहायता::TXT"

#: icewm-toolbar-icon-manager.sh:58
msgid ""
"What is this?\\nThis utility adds and removes application icons to IceWm's toolbar.\\nThe toolbar application icons are created from an application's .desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is created during an application's installation process to allow the system easy access to relevant information, such as the app's full name, commands to be executed, icon to be used, where it should be placed in the OS menu, etc.\\nA .desktop file name usually refers to the app's name, which makes it very easy to find the intended .desktop file (ex: Firefox ESR's .desktop file is 'firefox-esr.desktop').\\nWhen adding a new icon to the toolbar, the user can click the field presented in the main window and a list of all the .desktop files of the installed applications will be shown.\\nThat, in fact, is a list of (almost) all installed applications that can be added to the toolbar.\\nNote: some of antiX's applications are found in the sub-folder 'antiX'.\\n\n"
"TIM buttons:\\n 'ADD ICON' - select, from the list, the .desktop file of the application you want to add to your toolbar and it instantly shows up on the toolbar.\\nIf, for some reason, TIM fails to find the correct icon for your application, it will still create a toolbar icon using the default 'gears' image so that you can still click to access the application.\\nYou can click the 'Advanced' button to manually edit the relevant entry and change the application's icon.\\n'UNDO LAST STEP' - every time an icon is added or removed from the toolbar, TIM creates a backup file. If you click this button, the toolbar is instantly restored from that backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to remove its icon from the toolbar\\n'MOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to select it and then move it to the left or to the right\\n'ADVANCED' - allows for editing the text configuration file that has all of your desktop's toolbar icon's configurations. Manually editing this file allows the user to rearrange the order of the icons and delete or add any icon. A brief explanation about the inner workings of the text configuration file is displayed before the file is opened for editing.\\n Warnings: only manually edit a configuration file if you are sure of what you are doing! Always make a back up copy before editing a configuration file!"
msgstr ""
"यह क्या है?\\n यह आइस डब्लू-एम की साधन-पट्टी से अनुप्रयोग आइकन जोड़ने या हटाने हेतु एक साधन है।\\n साधन-पट्टी पर अनुप्रयोग आइकन का सृजन अनुप्रयोग की .desktop फाइल से होता है।\\n .desktop फाइल क्या होती है?\\n सामान्यतः अनुप्रयोग इंस्टॉल प्रक्रिया के समय सिस्टम हेतु सरल अभिगम के उद्देश्य से एक .desktop फाइल का सृजन होता है जिसमें अनुप्रयोग का पूर्ण नाम, निष्पादन की कमांड, प्रयुक्त आइकन व ऑपरेटिंग सिस्टम में स्थिति इत्यादि सूचना सम्मिलित होती है।\\n सामान्यतः इच्छित .desktop फाइल हेतु सरल सुगमता के कारण फाइल नाम अनुप्रयोग नाम के समान होता है (जैसे फायरफॉक्स विस्तृत समर्थन संस्करण की .desktop फाइल का नाम 'firefox-esr.desktop' है)।\\n साधन-पट्टी में नवीन आइकन जोड़ते समय उपयोक्ता मुख्य विंडो में दृश्यमान क्षेत्र पर क्लिक कर पर इंस्टॉल हो रखें अनुप्रयोगों की सभी .desktop फाइलें प्रदर्शित होंगी।\\n इस सूची में साधन-पट्टी में जोड़ने योग्य (लगभग) सभी इंस्टॉल हो रखें अनुप्रयोग प्रदर्शित होंगें।\\n ध्यान दें : एंटी-एक्स के कुछ अनुप्रयोग 'antiX' नामक उप-फोल्डर में मौजूद हैं।\\n\n"
"टीआईएम बटन :\\n 'आइकन जोड़ें' - सूची से साधन पट्टी पर जोड़ने हेतु इच्छित अनुप्रयोग की .desktop फाइल चुनें व यह तुरंत प्रभाव से साधन-पट्टी पर दृश्यमान होगी।\\n  किसी कारण से टीआईएम द्वारा अनुप्रयोग हेतु उचित आइकन की प्राप्ति विफल होने की स्थिति में डिफ़ॉल्ट 'Gears' आइकन प्रयुक्त कर साधन-पट्टी आइकन का सृजन होगा ताकि आप अनुप्रयोग हेतु अभिगम कर सकें।\\n संबंधित प्रविष्टि को स्वयं संपादित करने व अनुप्रयोग आइकन परिवर्तन हेतु 'विस्तृत' बटन पर क्लिक करें।\\n 'अंतिम परिवर्तन पूर्ववत करें' - साधन-पट्टी हेतु आइकन जोड़ने या हटाने के उपरांत प्रत्येक बार टीआईएम द्वारा एक बैकअप फाइल का सृजन होता है। इस बटन पर क्लिक करने से बिना पुष्टिकरण के ही तुरंत प्रभाव से बैकअप फाइल द्वारा साधन-पट्टी पुनः स्थापित हो जाएगी।\\n 'आइकन हटाएँ' - इससे उन सभी अनुप्रयोगों की सूची प्रदर्शित होगी जिनके आइकन साधन-पट्टी पर मौजूद हैं। साधन-पट्टी से हटाने हेतु इच्छित अनुप्रयोग पर दो बार बायाँ क्लिक करें।\\n 'आइकन अंतरण' - इससे उन सभी अनुप्रयोगों की सूची प्रदर्शित होगी जिनके आइकन साधन-पट्टी पर मौजूद हैं। इच्छित अनुप्रयोग को चयनित कर दायीं या बायीं ओर अंतरित करने हेतु उस पर दो बार बायाँ क्लिक करें।\\n 'विस्तृत' - इससे अपने डेस्कटॉप की साधन-पट्टी पर मौजूद सभी आइकन के विन्यास हेतु टेक्स्ट विन्यास फाइल का संपादन संभव है। उपयोक्ता द्वारा इस फाइल के संपादन से आइकन क्रम परिवर्तन व कोई भी आइकन जोड़ना या हटाना संभव है। संपादन हेतु फाइल खोलने से पूर्व टेक्स्ट विन्यास फाइल की आंतरिक कार्यप्रणाली का लघु विवरण प्रदर्शित होगा।\\n चेतावनी : इच्छित कार्य हेतु पूर्णतया निश्चित होने पर ही विन्यास फाइल का संपादन करें! विन्यास फाइल संपादन पूर्व एक बैकअप प्रतिलिपि अवश्य बनाएँ !"

#: icewm-toolbar-icon-manager.sh:67
msgid "Warning::TXT"
msgstr "चेतावनी::TXT"

#: icewm-toolbar-icon-manager.sh:67
msgid ""
"If you click 'Yes', the toolbar configuration file will be opened for manual editing.\\n\n"
"How-to:\\nEach toolbar icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each toolbar icon entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nSave any changes and then restart IceWM.\\nYou can undo the last change from TIMs UNDO LAST STEP button."
msgstr ""
"'हाँ' क्लिक करने पर संपादन हेतु साधन-पट्टी विन्यास फाइल खुलेगी।\\n\n"
"दिशानिर्देश :\\n प्रत्येक साधन-पट्टी आइकन की पहचान है 'prog' से आरंभ होने वाली पंक्ति जिसमें अनुप्रयोग नाम, आइकन स्थान व अनुप्रयोग संपादन फाइल मौजूद होती है।\\n प्रत्येक पर्सनल मेन्यू प्रविष्टि की ओर इंगित करने वाली संपूर्ण पंक्ति का अंतरण, संपादन या हटाना संभव है।\\n ध्यान दें : # से आरंभ होने वाली पंक्तियाँ केवल टिप्पणियाँ हैं व ये अनदेखी होंगी।\\n रिक्त पंक्तियाँ होना स्वीकार्य हैं।\\n इच्छित परिवर्तन संचित कर आइस डब्लू-एम को पुनः आरंभ करें।\\n अंतिम परिवर्तन को टीआईएम के 'पुनः स्थापना' बटन से पूर्ववत करना संभव है।"

#: icewm-toolbar-icon-manager.sh:83
msgid "Double click any Application to remove its icon:"
msgstr "आइकन हटाने हेतु संबंधित अनुप्रयोग पर द्वि-क्लिक करें :"

#: icewm-toolbar-icon-manager.sh:83
msgid "Remove"
msgstr "हटाएँ"

#: icewm-toolbar-icon-manager.sh:93
msgid "file has something"
msgstr "फाइल में सामग्री है"

#: icewm-toolbar-icon-manager.sh:99
msgid "file is empty"
msgstr "फाइल रिक्त है"

#: icewm-toolbar-icon-manager.sh:101
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"कोई परिवर्तन लागू नहीं किया गया!\\n सहायक निर्देश : विस्तृत बटन उपयोग करके "
"देखें।"

#: icewm-toolbar-icon-manager.sh:115
msgid "Double click any Application to move its icon:"
msgstr "आइकन अंतरण हेतु संबंधित अनुप्रयोग पर द्वि-क्लिक करें :"

#: icewm-toolbar-icon-manager.sh:115
msgid "Move"
msgstr "अंतरित करें"

#: icewm-toolbar-icon-manager.sh:125
msgid "nothing was selected"
msgstr "कुछ चयनित नहीं"

#: icewm-toolbar-icon-manager.sh:135
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "$EXEC आइकन हेतु कार्य चुनें"

#: icewm-toolbar-icon-manager.sh:137
msgid "Move left"
msgstr "बाएं अंतरित करें"

#: icewm-toolbar-icon-manager.sh:138
msgid "Move right"
msgstr "दाएं अंतरित करें"

#: icewm-toolbar-icon-manager.sh:204
msgid "Add selected app's icon"
msgstr "चयनित अनुप्रयोग का आइकन जोड़ें"

#: icewm-toolbar-icon-manager.sh:204
msgid "Choose application to add to the Toolbar"
msgstr "साधन-पट्टी में जोड़ने हेतु अनुप्रयोग चुनें"

#: icewm-toolbar-icon-manager.sh:259
msgid "No icon located, using default Gears icon"
msgstr "कोई आइकन नहीं मिला, डिफ़ॉल्ट Gears आइकन उपयोग होंगें"

#: icewm-toolbar-icon-manager.sh:261
msgid "Icon located!"
msgstr "आइकन प्राप्त हुआ!"

#: icewm-toolbar-icon-manager.sh:293
msgid "HELP!help:FBTN"
msgstr "सहायता!help:FBTN"

#: icewm-toolbar-icon-manager.sh:294
msgid "ADVANCED!help-hint:FBTN"
msgstr "विस्तृत!help-hint:FBTN"

#: icewm-toolbar-icon-manager.sh:295
msgid "ADD ICON!add:FBTN"
msgstr "आइकन जोड़ें!add:FBTN"

#: icewm-toolbar-icon-manager.sh:296
msgid "REMOVE ICON!remove:FBTN"
msgstr "आइकन हटाएँ!remove:FBTN"

#: icewm-toolbar-icon-manager.sh:297
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr "आइकन अंतरण!gtk-go-back-rtl:FBTN"

#: icewm-toolbar-icon-manager.sh:298
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "अंतिम परिवर्तन पूर्ववत करें!undo:FBTN"

#: icewm-toolbar-icon-manager.sh:299
msgid ""
"Please select any option from the buttons below to manage Toolbar icons"
msgstr "साधन पट्टी प्रबंधन हेतु नीचे दिए विकल्पों में से चयन करें"

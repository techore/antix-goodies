��          t      �         
             #     8     J     W     o     }  
   �     �  ~  �     3     G     M     j     �  %   �     �     �     �     �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2021
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Entrada automàtica Canvi Canvia el Gestor de Registre Canvia el fons de pantalla Usuari per omissió Activa el teclat numèric a l'entrada Gestor d’Inici de Sessió Trieu un Tema Prova el Tema Prova el tema abans d'usar-lo 
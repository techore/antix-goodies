��          t      �         
             #     8     J     W     o     }  
   �     �  r  �     '  
   :     E     `     x      �     �     �  	   �  &   �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Albertus Perkamentus, 2021
Language-Team: Dutch (https://www.transifex.com/anticapitalista/teams/10162/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Automatische login Veranderen Aanmeldingsbeheer wijzigen Verander de achtergrond Standaard gebruiker Numlock inschakelen bij inloggen Login Beheerder Selecteer thema Testthema Test het thema voordat je het gebruikt 
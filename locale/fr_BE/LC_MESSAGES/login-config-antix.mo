��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     N     j  ,   r     �     �  *   �  !   �          5  $   D                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 18:57+0300
Last-Translator: Wallon Wallon, 2021
Language-Team: French (Belgium) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Auto-login / auto-connexion Changer Changer le gestionnaire de connexion / login Changer le fond d'écran Utilisateur par défaut Activer le numlock à la connexion / login Gestionnaire de connexion / login Sélectionner le thème Thème d'essai Tester le thème avant de l'utiliser 